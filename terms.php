<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="Class Connect Jr. is an advanced Learning Management System for all kinds of institutions to conduct academic activities online. This LMS provides a paperless automation solution system with a low price.">
    <meta name="keywords" content="learning management system bangladesh, education management, best, school management, e-learning, primary school, online education, lms, moodle, Top learning management system">
    <meta name="author" content="Medina Tech">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Class Connect Jr. | Learning Management System | e-learning management system</title>

    <!-- Bootstrap Min CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- Meanmenu Min CSS -->
    <link rel="stylesheet" href="assets/css/meanmenu.css">
    <!-- Magnific Popup Min CSS -->
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="assets/css/odometer.css">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="assets/css/slick.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- resources CSS -->
    <link rel="stylesheet" href="assets/css/resources.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>

<body>

    <!-- Start Preloader Area -->
    <!--<div class="preloader">
    <div class="spinner"></div>
</div>-->
    <!-- End Preloader Area -->

    <!-- Start Navbar Area -->
    <header id="header">
        <div class="startp-mobile-nav">
            <div class="logo">
                <a href="https://classconnect.live/"><img src="assets/img/logo.png" class="logoImg" alt="logo"></a>
            </div>
        </div>

        <div class="startp-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="https://classconnect.live/"><img src="assets/img/logo.png" class="logoImg" alt="logo"></a>

                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav nav ml-auto">

                            <li class="nav-item"><a href="#About" data-translatekey="about" rel='m_PageScroll2id'>About</a></li>
                            <li class="nav-item"><a href="#Speciality" data-translatekey="our-speciality" rel='m_PageScroll2id'>Our Specialty</a></li>
                            <li class="nav-item"><a href="#Features" data-translatekey="features" rel='m_PageScroll2id'>Features</a></li>
                            <li class="nav-item"><a href="#Talk" data-translatekey="demo" rel='m_PageScroll2id'>Demo</a></li>
                            <!-- <li class="nav-item"><a href="#Package" data-translatekey="packages" rel='m_PageScroll2id'>Packages</a></li> -->
                            <li class="nav-item"><a href="#Sponsor" data-translatekey="sponsor" rel='m_PageScroll2id'>Sponsor</a></li>
                            <li class="nav-item"><a href="#Contact" data-translatekey="contact" rel='m_PageScroll2id'>Contact</a></li>
                        </ul>
                    </div>

                    <div class="others-option">
                        <!-- <div class="langWrap">
                            <a onclick="englishMode()" href="#" language='english' id="en" class="active">English</a>
                            <a onclick="banglaMode()" href="#" language='bangla' id="bd">বাংলা</a>
                        </div> -->
                        <div class="language_change">
                            <div class="switch">
                                <div class="language">
                                    <input onclick="englishMode()" id="q1" name="locale" type="radio" value="en" checked>
                                    <label for="q1">English</label>
                                </div>
                                <div class="language">
                                    <input onclick="banglaMode()" id="q2" name="locale" type="radio" value="bn">
                                    <label for="q2">বাংলা</label>
                                </div>
                            </div>
                        </div>
                        <!--<div class="langWrap">
                      <a href="index.php" language='english' class="active">English</a>
                      <a href="indexbd.html" language='bangla'>বাংলা</a>
                  </div>-->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- End Navbar Area -->

    <!-- Terms Body Start -->
    <div class="blog-details-area ptb-80 marginUpper">
        <div class="container">
            <div class="section-title">
                <h2 data-translatekey="user-terms">User Terms</h2>
                <div class="bar"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="blog-details-desc">
                        <div class="article-content">
                            <div class="entry-meta">
                                <h4 class="upDownMargin" data-translatekey="purpose-overview">Purpose and Overview (Class Connect Jr.)</h4>

                                <p data-translatekey="to-define-lms">
                                    To define the Standard Procedures and Guidelines that governs and promotes the efficient use of the Learning Management System (LMS) licensed by the Class Connect Jr.
                                </p>

                                <p data-translatekey="goal-of-lms">
                                    Class Connect Jr. assists schools and colleges to handle academic dependency into a single platform with multiple users panel. The goal of this education software system is to alter the interaction among students, teachers and administrative data simultaneously.
                                    According to this aspect, it can enable teachers to create customized exams for students, accessible and submitted online.
                                </p>

                                <p data-translatekey="the-positive-side">
                                    The positive side for this system is that it can be used by any school, college or university to forward their educational tasks online throughout a pandemic. This Learning Management System is favorable, highly secure to store users academic information
                                    and accessible as a result of it brings along the administration, students and course teachers below one single system. It aids in reducing the paper work of schools and colleges that they need to manage on a daily
                                    basis. The system came up with a solution: the teachers and therefore the students will keep track of the continuing activities of the school or college. Moreover, the system provides a useful user friendly system and
                                    it is quite easy to maintain for the Admin.
                                </p>
                            </div>
                            <div class="entry-meta">
                                <h4 class="upDownMargin" data-translatekey="management-and-administration">Class Connect Jr. Management and Administration</h4>
                                <ul>
                                    <li data-translatekey="medinatech-resposible">
                                        “Medina Tech” is responsible for the administration of the Class Connect Jr.
                                    </li>

                                    <li data-translatekey="class-connect-responsible">
                                        The Class Connect Jr. Administrator is responsible for the management and administration of all aspects of the learning management system including but not limited to:
                                    </li>
                                    <ol type="a">
                                        <li data-translatekey="user-interface-components">
                                            User interface components and design, navigation links, and tool configuration and availability.
                                        </li>
                                        <li data-translatekey="course-components">
                                            Course components including site design and structure, course codes and term designations.
                                        </li>
                                        <li data-translatekey="external-learning-tool">
                                            External Learning Tools and other services integration.
                                        </li>
                                    </ol>
                                    <li data-translatekey="request-for-change">
                                        Requests for changes to standard templates and configurations within the learning management system will be made to the Class Connect Jr Administrator.Requests will be assessed by Class Connect Jr. Administrator, and if approved will follow standard web
                                        design practices and principles for usability and accessibility.
                                    </li>
                                    </ol>
                            </div>
                            <div class="entry-meta">
                                <h4 class="upDownMargin" data-translatekey="user-management-access">User Management and Access (Class Connect Jr.)</h4>
                                <ul>
                                    <li data-translatekey="all-user-must-authenticat">
                                        All Users must be authenticated with unique login credentials, and the Class Connect Jr. is usable for specific schools or college academic purposes only.
                                    </li>
                                    <li data-translatekey="all-user-including-student">
                                        All Users including Students, Teachers and Admin must access the system through an assigned login credential which will be generated from the schools or college.
                                    </li>
                                    <li data-translatekey="student-gurdians-will">
                                        Student Guardians will have their own Guardians Panel into the system. Guardians will also have access to login into their panel, by using their own login credentials such as User Name and Password. These credentials will be unique than others and these
                                        credentials will be provided from the institution. Schools Authority will handle and track this login accessibility.
                                    </li>
                                    <li data-translatekey="ensure-privacy">
                                        In order to ensure privacy, protection of academic data and the integrity of materials, access to courses in the class Connect Jr. is regulated.
                                    </li>
                                    <li data-translatekey="other-may-granted-access">
                                        School Admin or other employees may be granted access to courses or subject lists when requested by the course teacher for educational and advisory purposes. These requests for access must be forwarded to the Class Connect Jr. Administrator.
                                    </li>
                                    <li data-translatekey="accordingly-ccjr">
                                        Accordingly, access to Class Connect Jr. courses will only be by approved, and authenticated users who require access to specific courses according to the role and responsibility of each User and only for a some period of time.
                                    </li>
                                </ul>
                            </div>
                            <h4 class="upDownMargin" data-translatekey="policy-statement">Policy Statement (Class Connect Jr.)</h4>
                            <p data-translatekey="p-access-lms">
                                <b data-translatekey="b-access-lms">Access</b> - Access to LMS software, materials, and online video class will be granted as follows:
                            </p>
                            <ul>
                                <li data-translatekey="li-one-user-account">
                                    <b data-translatekey="b-one-user-account">One User Account</b> - All the Admin from the school, Course Teacher, Students, and Parents each have one account on each LMS that will be used for all of their LMS activities.
                                    Login credentials will be provided from the schools for every particular user and this information will be secured for the authentication and validation part. Even without the login credentials Students and Teachers
                                    won’t be able to get access into the Learning Management System and won’t be able to view their individual account information.
                                </li>
                                <li data-translatekey="li-student-access">
                                    <b data-translatekey="b-student-access">Student Access</b> - Students who have personal login credentials will be able to enter the system. Same as Teachers, the students login credentials will be provided from the
                                    institution. These credentials will help to access the class materials, syllabus, student profile information, grades, and most importantly using this student will be able to do his online classes. According to this
                                    system, the course teacher will record the student's attendance and this information will also be recorded in the system. Every student will be assigned their own Class. So, all the Subjects, Class Routine are always
                                    generated by the School Admin. Moreover, after login into the system, students will be able to access their assigned class automatically.
                                </li>
                                <li data-translatekey="li-teacher-access">
                                    <b data-translatekey="b-teacher-access">Teacher Access</b> - This Learning Management System will provide a teaching panel and according to this system, all the course teachers will be able to conduct their classes
                                    in time, upload assignments and grades simultaneously. This system will assist everyone to send and receive messages from the teacher and the students. The Teachers can add text, images, tables, links and text formatting,
                                    interactive tests, slideshows etc. Teachers can manage courses and course modules, class syllabus, and can see attendance reports on students from their online classes.
                                </li>
                                <li data-translatekey="li-guardian-access">
                                    <b data-translatekey="b-gurdian-access">Guardian Access</b> - Every Parent will have access to enter the LMS with their own Parents credentials. This system will ensure they know their kids' performance, class attendance
                                    and grades. They can view exam dates, calendars and overall performance from this system.
                                </li>
                                <li data-translatekey="li-length-access">
                                    <b data-translatekey="b-length-access">Length of Access to Course Site</b> - Students have access to their course site(s) until an instructor makes the course unavailable. This is typically done at the end of a term,
                                    but is at the discretion of the instructor.
                                </li>
                            </ul>
                            <h4 class="upDownMargin" data-translatekey="data-privacy">Data and Privacy (Class Connect Jr.)</h4>
                            <p data-translatekey="p-system-administration">
                                <strong data-translatekey="strong-system-administration">System Administration :</strong> The Class Connect Jr. Administrators make every effort to minimize the amount of personal information about students, teachers and
                                guardians that resides on the system. They are also very concerned regarding the Admin information of the institutes. Therefore, few general Information used to create user accounts includes first name, nick name, e-mail
                                address, address and phone number. This sort of user information won’t be shared with others or won’t be used illegally. But, Class Connect Jr. Administrators may have used this data further for the data analytics part
                                to improve the learning management system.
                            </p>
                            <p data-translatekey="p-student-privacy">
                                <strong data-translatekey="strong-student-privacy">Student Privacy :</strong> Faculty, Students, and School Admin should become familiar with the guidelines regarding education records. Faculty should consider student privacy
                                issues when making choices regarding access to areas in their courses for users not enrolled in the courses. Areas of concern include Grades, Class Attendances, Questions Papers, Personal Information, and Class Roster.
                            </p>
                            <p data-translatekey="p-conent-privacy">
                                <strong data-translatekey="strong-content-privacy">Content Privacy :</strong> Class Connect Jr. is a learning management system so this system is a storage system. All the information should be stored as backup and the
                                authenticated users such as School Admin, Teachers will be able to reuse that data later. This information won’t be accessible without the login credentials and the login credentials will be secured by the system. To secure
                                content into the system, every user should have maintained secure credentials.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Terms Body End -->

    <!-- Start Footer Area -->
    <footer class="footer-area bg-f7fafd" id="Footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget">
                        <div class="logo">
                            <a href="http://classconnect.live"><img src="assets/img/logo.png" alt="logo"></a>
                        </div>
                        <p data-translatekey="footer-desc">Class Connect is a Learning Management System designed to enhance your Online Teaching & Learning Experience.
                        </p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget pl-5">
                        <h3 data-translatekey="company">Company</h3>
                        <ul class="list">
                            <li><a href="#" data-translatekey="about-us">About Us</a></li>
                            <li><a href="#" data-translatekey="services">Services</a></li>
                            <li><a href="#" data-translatekey="feature">Features</a></li>
                            <li><a href="#" data-translatekey="pricing">Our Pricing</a></li>
                            <li><a href="#" data-translatekey="latest-news">Latest News</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget">
                        <h3 data-translatekey="support">Support</h3>
                        <ul class="list">
                            <!-- <li><a href="#" data-translatekey="faq">FAQ's</a></li> -->
                            <li><a href="#" data-translatekey="privacy-policy">Privacy Policy</a></li>
                            <li><a href="terms.php" data-translatekey="user-terms">User Terms</a></li>
                            <!-- <li><a href="#" data-translatekey="community">Community</a></li> -->
                            <!-- <li><a href="#" data-translatekey="contact-us">Contact Us</a></li> -->
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget">
                        <h3 data-translatekey="address">Address</h3>

                        <ul class="footer-contact-info">
                            <li><i data-feather="map-pin"></i> <label data-translatekey="location">House #25, Road #4, Block #F, Banani, Dhaka 1213</label></li>
                            <li><i data-feather="mail"></i> <label data-translatekey="email-txt"></label> <a data-translatekey="email" href="mailto:support@medinatech.co">support@medinatech.co</a>
                            </li>
                            <li><i data-feather="phone-call"></i> <label data-translatekey="phone-txt"></label><a href="tel:+8809638600700" data-translatekey="phone">+88 0963 8600 700</a>
                            </li>
                        </ul>
                        <ul class="social-links">
                            <li><a href="https://www.facebook.com/Class-Connect-100120898989894" class="facebook"><i data-feather="facebook" aria-level="facebook"></i></a></li>
                            <!-- <li><a href="#" class="twitter"><i data-feather="twitter"></i></a></li> -->
                            <!-- <li><a href="#" class="instagram"><i data-feather="instagram"></i></a></li> -->
<!--                            <li><a href="#" class="linkedin"><i data-feather="linkedin" aria-level="linkedin"></i></a></li>-->
                        </ul>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="copyright-area">
                        <p data-translatekey="footer-text">Copyright @2021 Medina Tech. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>

        <img src="assets/img/map.png" class="map" alt="map">
        <div class="shape1"><img src="assets/img/shape1.png" alt="shape"></div>
        <div class="shape8 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
    </footer>
    <!-- End Footer Area -->

    <div class="go-top"><i data-feather="arrow-up"></i></div>

    <!-- Jquery Min JS -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Popper Min JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap Min JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Meanmenu Min JS -->
    <script src="assets/js/jquery.meanmenu.min.js"></script>
    <!-- WOW Min JS -->
    <script src="assets/js/wow.min.js"></script>
    <!-- Magnific Popup Min JS -->
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <!-- Appear Min JS -->
    <script src="assets/js/jquery.appear.js"></script>
    <!-- Odometer Min JS -->
    <script src="assets/js/odometer.min.js"></script>
    <!-- Slick Min JS -->
    <script src="assets/js/slick.js"></script>
    <!-- Owl Carousel Min JS -->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!-- Feather Icon Min JS -->
    <script src="assets/js/feather.min.js"></script>
    <!-- Form Validator Min JS -->
    <script src="assets/js/form-validator.min.js"></script>
    <!-- Contact Form Min JS -->
    <script src="assets/js/contact-form-script.js"></script>
    <!-- StartP Map JS FILE -->
    <script src="assets/js/startp-map.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
    <script src="assets/js/tinyi18n.js"></script>
    <script>
        tinyi18n.loadTranslations('language-terms.json');
    </script>
     <script>
        function englishMode() {
            tinyi18n.setLang('en');
        }
    </script>
    <script>
        function banglaMode() {
            tinyi18n.setLang('bd');
        }
    </script>

    <!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "100120898989894");
        chatbox.setAttribute("attribution", "biz_inbox");
        window.fbAsyncInit = function() {
            FB.init({
                xfbml            : true,
                version          : 'v11.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</body>

</html>