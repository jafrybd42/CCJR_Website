<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description"
          content="Class Connect Jr. is an advanced Learning Management System for all kinds of institutions to conduct academic activities online. This LMS provides a paperless automation solution system with a low price.">
    <meta name="keywords"
          content="learning management system bangladesh, education management, best, school management, e-learning, primary school, online education, lms, moodle, Top learning management system">
    <meta name="author" content="Medina Tech">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Class Connect Jr. | Learning Management System | e-learning management system</title>
    <!-- Bootstrap Min CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- Meanmenu Min CSS -->
    <link rel="stylesheet" href="assets/css/meanmenu.css">
    <!-- Magnific Popup Min CSS -->
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="assets/css/odometer.css">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="assets/css/slick.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- resources CSS -->
    <link rel="stylesheet" href="assets/css/resources.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-1RF28FJ0CE"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-1RF28FJ0CE');
    </script>
</head>

<body>

<!-- Start Preloader Area -->
<div class="preloader">
    <div class="spinner"></div>
</div>
<!-- End Preloader Area -->

<!-- Start Navbar Area -->
<header id="header">
    <div class="startp-mobile-nav">
        <div class="logo">
            <a href="https://classconnect.live/"><img src="assets/img/logo.png" class="logoImg" alt="logo"></a>
        </div>
        <div class="language_change">
            <div class="switch">
                <div class="language">
                    <input onclick="englishMode()" id="q3" name="locale" type="radio" value="en" checked>
                    <label for="q3">English</label>
                </div>
                <div class="language">
                    <input onclick="banglaMode()" id="q4" name="locale" type="radio" value="bn">
                    <label for="q4">বাংলা</label>
                </div>
            </div>
        </div>
    </div>

    <div class="startp-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="https://classconnect.live/">
                    <img src="assets/img/logo.png" class="logoImg" alt="logo">
                </a>

                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav ml-auto">

                        <li class="nav-item"><a href="#About" rel='m_PageScroll2id' data-translatekey="about">About</a>
                        </li>
                        <li class="nav-item"><a href="#Speciality" rel='m_PageScroll2id'
                                                data-translatekey="our-speciality">Our Specialty</a></li>
                        <li class="nav-item"><a href="#Features" rel='m_PageScroll2id' data-translatekey="feature">Features</a>
                        </li>
                        <li class="nav-item"><a href="#Talk" rel='m_PageScroll2id' data-translatekey="demo">Demo</a>
                        </li>
                        <!-- <li class="nav-item"><a href="#Package" rel='m_PageScroll2id' data-translatekey="packages">Packages</a></li> -->
                        <li class="nav-item"><a href="#Sponsor" rel='m_PageScroll2id' data-translatekey="sponsor">Sponsor</a>
                        </li>
                        <li class="nav-item"><a href="#Contact" rel='m_PageScroll2id' data-translatekey="contact">Contact</a>
                        </li>

                    </ul>


                </div>

                <div class="others-option">
                    <div class="language_change">
                        <div class="switch">
                            <div class="language">
                                <input onclick="englishMode()" id="q1" name="locale" type="radio" value="en" checked>
                                <label for="q1">English</label>
                            </div>
                            <div class="language">
                                <input onclick="banglaMode()" id="q2" name="locale" type="radio" value="bn">
                                <label for="q2">বাংলা</label>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="langWrap">
                        <a onclick="englishMode()" href="#" language='english' value="en" class="active">English</a>
                        <a onclick="banglaMode()" href="#" language='bangla' value="zh">বাংলা</a>
                    </div> -->
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- End Navbar Area -->

<!-- Start Main Banner -->
<div class="main-banner" id="About">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="row h-100 justify-content-center align-items-center">

                    <div class="col-lg-6">
                        <div class="banner-image">
                            <img src="assets/img/landing-gfx/sir.png" class="wow fadeInLeft gfx_sir"
                                 data-wow-delay="0.3s" alt="man">
                            <img src="assets/img/landing-gfx/student.png" class="wow fadeInRight gfx_student"
                                 data-wow-delay="0.5s" alt="code">
                            <img src="assets/img/landing-gfx/room.png" class="wow fadeInUp gfx_room"
                                 data-wow-delay="0.6s" alt="carpet">
                            <img src="assets/img/landing-gfx/full.png" class="wow fadeInUp gfx_full"
                                 data-wow-delay="0.3s" alt="main-pic"
                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">

                        </div>
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <div class="hero-content">
                            <h3 data-translatekey="title">Learning Management System</h3>
                            <div class="text-container" id="tt-en">
                                <p class="typewriter">
                                    <span class="typewriter-text"
                                          data-translatekey='[ "Class Connect", "All In Here ", "Live Class ", "Attendance ","Online Exam ", "Online Payment" ]'></span>
                                </p>
                            </div>
                            <div class="text-container tt-none" id="tt-bn">
                                <p class="typewriter">
                                    <span class="typewriter-text-bd"
                                          data-text='[ "ক্লাস কানেক্ট", "সব মিলিয়ে এখানে", "লাইভ ক্লাস", "এ্যাটেনডেন্স", "অনলাইন পরীক্ষার", "অনলাইন পেমেন্ট"]'></span>
                                </p>
                            </div>
                            <p data-translatekey="description">Class Connect is a Learning Management System designed to
                                enhance your Online Teaching & Learning Experience with some of the best user experience
                                features for all types of Education boards of Bangladesh
                            </p>

                            <!-- <a href="#" class="btn btn-primary">Get Started</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="shape1"><img src="assets/img/shape1.png" alt="shape"></div>
    <div class="shape2 rotateme"><img src="assets/img/shape2.png" alt="shape"></div>
    <div class="shape3"><img src="assets/img/shape3.png" alt="shape"></div>
    <div class="shape4"><img src="assets/img/shape7.png" alt="shape"></div>
    <div class="shape5"><img src="assets/img/shape5.png" alt="shape"></div>
    <div class="shape6 rotateme"><img src="assets/img/shape4.png" alt="shape"></div>
    <div class="shape7"><img src="assets/img/shape4.png" alt="shape"></div>
    <div class="shape8 rotateme"><img src="assets/img/shape6.png" alt="shape"></div>
</div>
<!-- End Main Banner -->


<!-- Start Speciality Area -->
<section class="boxes-area ptb-80" id="Speciality">
    <div class="container">
        <h3 data-translatekey="speciality"> Our Speciality</h3>
        <br/>
        <div class="row">
            <div class="boxes-slides">
                <div class="col-lg-12 col-md-6">
                    <div class="single-box">
                        <div class="icon">
                            <i data-feather="video"></i>
                        </div>
                        <h3 data-translatekey="online-video-class">Online Video Class</h3>
                        <p data-translatekey="online-video-desc">We have our own Video conferencing tool that allows you
                            to conduct online lectures, group conversations & recordings.</p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-6">
                    <div class="single-box bg-f78acb">
                        <div class="icon">
                            <i data-feather="globe"></i>
                        </div>
                        <h3 data-translatekey="bangla-english">Bangla & English</h3>
                        <p data-translatekey="bangla-english-desc">Our users can experience usability convenience in
                            managing content in both Bangla & English. Our Technical Stack also allows us to add other
                            additional languages.</p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-6">
                    <div class="single-box bg-c679e3">
                        <div class="icon">
                            <i data-feather="hash"></i>
                        </div>
                        <h3 data-translatekey="online-exams">Online Exams</h3>
                        <p data-translatekey="online-exams-desc">We customize our Exam Module with respect to school &
                            board needs to provide an authentic online testing experience. </p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-6">
                    <div class="single-box bg-eb6b3d">
                        <div class="icon">
                            <i data-feather="compass"></i>
                        </div>
                        <h3 data-translatekey="academic-history">Academic History Tracking</h3>
                        <p data-translatekey="academic-history-desc">Prioritizing long term value, we ensure that
                            student information is maintained in a way that their history becomes useful as they move
                            from one academic year to the next and after becoming a school alumni.</p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="single-box bg-eb6b3d">
                        <div class="icon">
                            <i data-feather="settings"></i>
                        </div>
                        <h3 data-translatekey="training">Trainings for Onboarding & Maintenance</h3>
                        <p data-translatekey="training-desc">We will provide in person training sessions, online
                            documents & videos for: Admin Training for Setup & Maintenance Teachers, Students &
                            Guardians User Specific Training

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- End Boxes Area -->


<!-- Start Responsive Area -->
<section class="iot-features-area bg-f7fafd" id="Responsive">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 iot-features-content">
                <h3 data-translatekey="web-responsive">Web & Mobile Responsive</h3>
                <div class="bar"></div>
                <p data-translatekey="web-responsive-desc">Attempts to provide the user with the ultimate seamless
                    experience between the mobile and desktop versions of their website.</p>

                <!-- <a href="#" class="btn btn-primary">Explore More</a> -->
            </div>
            <div class="col-lg-6 iot-features-image">
                <img src="assets/img/iot-features-image/ccjr.png" class="wow fadeInUp devicesImg" data-wow-delay="0.3s"
                     alt="image">
            </div>
        </div>
    </div>
</section>
<!-- End IoT Features Area -->

<!-- Start Security & Accessibility Area -->
<section class="iot-features-area" id="Security">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-lg-6 iot-features-image">
                <img src="assets/img/iot-features-image/security.png" class="wow fadeInUp" data-wow-delay="0.3s"
                     alt="image">
            </div>
            <div class="col-lg-6 iot-features-content">
                <h3 data-translatekey="security-accessibility">Security & Accessibility</h3>
                <div class="bar"></div>
                <p data-translatekey="security-accessibility-desc">We operate on a 'zero trust' mindset, ensuring the
                    safest and more secure systems are in place within our organisation. Cutting edge technology and
                    regular updates ensure we are ahead of the curve.</p>
            </div>
        </div>
    </div>
</section>
<!-- End IoT Features Area -->

<!-- Start Features Area -->
<section class="features-area ptb-80 bg-f7fafd" id="Features">
    <div class="container">
        <div class="section-title">
            <h2 data-translatekey="features">Our Features</h2>
            <div class="bar"></div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon">
                        <i data-feather="settings"></i>
                    </div>

                    <h3 data-translatekey="Management">Management</h3>
                    <p data-translatekey="Management-desc">Student, Teacher & Guardian Management.</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon">
                        <i data-feather="mail"></i>
                    </div>

                    <h3 data-translatekey="class-course-management">Class & Course Management</h3>
                    <p data-translatekey="class-course-management-desc">Class Routines, Attendance, Lectures, Notes,
                        Assignments, Exams, Quizzes & Report Cards. </p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-c679e3">
                        <i data-feather="video"></i>
                    </div>

                    <h3 data-translatekey="live-class">Online Live Class</h3>
                    <p data-translatekey="live-class-desc">Students will get live class opportunity.</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-c679e3">
                        <i data-feather="info"></i>
                    </div>

                    <h3 data-translatekey="school-communication">School Communications</h3>
                    <p data-translatekey="school-communication-desc"> Admissions, Notice Boards, Academic Calendars &
                        Syllabus.</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-eb6b3d">
                        <i data-feather="message-square"></i>
                    </div>

                    <h3 data-translatekey="Communication">Communication</h3>
                    <p data-translatekey="Communication-desc">Student - Teacher Messaging Platform.</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-eb6b3d">
                        <i data-feather="dollar-sign"></i>
                    </div>

                    <h3 data-translatekey="payment-tracking">Online Payment & Tracking</h3>
                    <p data-translatekey="payment-tracking-desc">Let students pay their fees on online and track all the
                        payments.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-eb6b3d">
                        <i data-feather="bell"></i>
                    </div>

                    <h3 data-translatekey="Notification-system">Notification System</h3>
                    <p data-translatekey="Notification-system-desc">Students and Teacher will get Notification for
                        different types of activities.</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="single-features">
                    <div class="icon bg-eb6b3d">
                        <i data-feather="message-circle"></i>
                    </div>

                    <h3 data-translatekey="chat-bot">Helper Chat Bot</h3>
                    <p data-translatekey="chat-bot-desc">Chat bot will help student about how to use Class Connect.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Features Area -->


<!-- Start Ready To Talk Area -->
<section class="ready-to-talk" id="Talk">
    <div class="container">
        <h3 data-translatekey="ready-talk">Ready to talk?</h3>
        <p data-translatekey="ready-talk-desc">Our team is here to answer your question about Class Connect</p>
        <a data-translatekey="contact-us" href="#Contact" class="btn btn-primary">Contact Us</a>
    </div>
</section>
<!-- End Ready To Talk Area -->


<div class="about-area ptb-80">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
                <div class="ml-about-content">
                    <!--<span class="sub-title">About Us</span>-->
                    <h2 data-translatekey="what-problem">What is the problem?</h2>
                    <div class="bar"></div>
                    <p data-translatekey="what-problem-desc">Majority of Schools in Bangladesh do not have a proper
                        Learning Management System regardless of the Education Board they are following. The abundance
                        of data & need for mobility every School, Teacher & Student have to manage, store
                        & organize their learning materials and records everyday Therefore, there is a dire need of
                        proper data management tools in schools.
                    </p>
                    <!--<a href="#" class="btn btn-secondary">Discover More</a>-->
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="ml-about-img text-center">
                    <img src="assets/img/about4.png" alt="image" class="wow fadeInUp" data-wow-delay="0.3s" alt="image">
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Why Medina Tech -->
<div class="ml-services-area ptb-80">
    <div class="container">
        <div class="section-title">
            <img class="mb-3" src="assets/img/logo2.png" alt="Medina Tech">
            <h3 data-translatekey="why-medinatech">Why Medina Tech?</h3>
            <div class="bar"></div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-md-6">
                <div class="single-ml-services-box">
                    <div class="image">
                        <img src="assets/img/services-left-image/1.png" alt="image">
                    </div>
                    <h5 class="my-3" data-translatekey="customization">Customization</h5>
                    <p data-translatekey="customization-desc">Customizing to provide the best User Experience is a big
                        priority for us, therefore our in house UI/UX Designers, Software Engineers and Data Scientists
                        work together to provide the best customization possible.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-6">
                <div class="single-ml-services-box">
                    <div class="image">
                        <img src="assets/img/services-left-image/4.png" alt="image">
                    </div>
                    <h5 class="my-3" data-translatekey="security-surveillance">Security &amp; Surveillance</h5>
                    <p data-translatekey="security-surveillance-desc">Storing facility is developed using advanced Cloud
                        based platform, to ensure that schools can get flexibility in terms of the quantity of data they
                        want to store & process. </p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-6">
                <div class="single-ml-services-box">
                    <div class="image">
                        <img src="assets/img/services-left-image/6.png" alt="image">
                    </div>
                    <h5 class="my-3" data-translatekey="data-visualization">Data Visualization</h5>
                    <p data-translatekey="data-visualization-desc">Schools can access real-time MIS reports and
                        Analytical data, anywhere. With the advancement of AI, Big Data, and Augmented Reality, the LMS
                        is getting better and better. Our perception of reality is changing every day as we move toward
                        the future.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="shape1"><img src="assets/img/shape1.png" alt="shape"></div>
    <div class="shape2 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
    <div class="shape3"><img src="assets/img/shape3.svg" alt="shape"></div>
    <div class="shape4"><img src="assets/img/shape4.svg" alt="shape"></div>
    <div class="shape6 rotateme"><img src="assets/img/shape4.svg" alt="shape"></div>
    <div class="shape7"><img src="assets/img/shape4.svg" alt="shape"></div>
    <div class="shape8 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
</div>
<!-- -->


<!-- Start Pricing Area -->
<!--<section class="pricing-area ptb-80 bg-f9f6f6" id="Package">
<div class="container">
    <div class="section-title">
        <h2>Pricing Plans</h2>
        <div class="bar"></div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="pricing-table">
                <div class="pricing-header">
                    <h3>Basic Plan</h3>
                </div>

                <div class="price">
                    <span><sup>$</sup>15.00 <span>/Mon</span></span>
                </div>

                <div class="pricing-features">
                    <ul>
                        <li class="active">5 GB Bandwidth</li>
                        <li class="active">Highest Speed</li>
                        <li class="active">1 GB Storage</li>
                        <li class="active">Unlimited Website</li>
                        <li class="active">Unlimited Users</li>
                        <li class="active">24x7 Great Support</li>
                        <li>Data Security and Backups</li>
                        <li>Monthly Reports and Analytics</li>
                    </ul>
                </div>

                <div class="pricing-footer">
                    <a href="#" class="btn btn-primary">Select Plan</a>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="pricing-table">
                <div class="pricing-header">
                    <h3>Advanced Plan</h3>
                </div>

                <div class="price">
                    <span><sup>$</sup>35.00 <span>/Mon</span></span>
                </div>

                <div class="pricing-features">
                    <ul>
                        <li class="active">10 GB Bandwidth</li>
                        <li class="active">Highest Speed</li>
                        <li class="active">3 GB Storage</li>
                        <li class="active">Unlimited Website</li>
                        <li class="active">Unlimited Users</li>
                        <li class="active">24x7 Great Support</li>
                        <li class="active">Data Security and Backups</li>
                        <li>Monthly Reports and Analytics</li>
                    </ul>
                </div>

                <div class="pricing-footer">
                    <a href="#" class="btn btn-primary">Select Plan</a>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
            <div class="pricing-table">
                <div class="pricing-header">
                    <h3>Expert Plan</h3>
                </div>

                <div class="price">
                    <span><sup>$</sup>65.00 <span>/Mon</span></span>
                </div>

                <div class="pricing-features">
                    <ul>
                        <li class="active">15 GB Bandwidth</li>
                        <li class="active">Highest Speed</li>
                        <li class="active">5 GB Storage</li>
                        <li class="active">Unlimited Website</li>
                        <li class="active">Unlimited Users</li>
                        <li class="active">24x7 Great Support</li>
                        <li class="active">Data Security and Backups</li>
                        <li class="active">Monthly Reports and Analytics</li>
                    </ul>
                </div>

                <div class="pricing-footer">
                    <a href="#" class="btn btn-primary">Select Plan</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="shape8 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
<div class="shape2 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
<div class="shape7"><img src="assets/img/shape4.svg" alt="shape"></div>
<div class="shape4"><img src="assets/img/shape4.svg" alt="shape"></div>
</section>-->
<!-- End Pricing Area -->


<section class="repair-cta-area bg-0f054b" id="Sponsor">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12 col-lg-8">
                <div class="cta-repair-content">
                    <h3 data-translatekey="what-need">Why we need this?</h3>
                    <p data-translatekey="what-need-desc">In order to create Digital Bangladesh from the root level of
                        society, the Education Systems needs Learning & School Management System at some point starting
                        now!</p>
                    <div class="row services-content">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <span data-translatekey="organizes-content">Organizes content in one location</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <span data-translatekey="storage-learning-metarials">Secures storage of learning materials and user records</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <Span data-translatekey="up-to-date">Keeps organizations up-to-date with compliance regulations</Span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <span data-translatekey="streamlines-facilities">Streamlines and Facilitates the Admission Process</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <Span data-translatekey="track-learning-paths">Tracks Students’ Learning Paths</Span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="box">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-code">
                                    <polyline points="16 18 22 12 16 6"></polyline>
                                    <polyline points="8 6 2 12 8 18"></polyline>
                                </svg>
                                <span data-translatekey="save-times-money">Saves Time And Money</span>
                            </div>
                        </div>
                    </div>
                    <!--<a href="#" class="btn btn-primary">Contact Us</a>-->
                </div>
            </div>

            <div class="col-md-12 col-lg-3 offset-lg-1">
                <div class="cta-repair-img">
                    <div class="text-center">
                        <p class="text-white">
                        <h3 class="text-white" data-translatekey="Approximately">Approximately</h3>
                        <div class="funfact">
                            <h3>
                                    <span class="odometer odometer-auto-theme" data-count="100">
                                        <div class="odometer-inside">
                                            <span class="odometer-digit">
                                                <span class="odometer-digit-spacer">8</span>
                                                <span class="odometer-digit-inner">
                                                    <span class="odometer-ribbon">
                                                        <span class="odometer-ribbon-inner">
                                                            <span class="odometer-value">1</span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="odometer-digit">
                                                <span class="odometer-digit-spacer">10</span>
                                                <span class="odometer-digit-inner">
                                                    <span class="odometer-ribbon">
                                                        <span class="odometer-ribbon-inner">
                                                            <span class="odometer-value">8</span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="odometer-digit">
                                                <span class="odometer-digit-spacer">10</span>
                                                <span class="odometer-digit-inner">
                                                    <span class="odometer-ribbon">
                                                        <span class="odometer-ribbon-inner">
                                                            <span class="odometer-value">0</span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                        </div>
                                    </span>K+
                            </h3>
                            <p class="text-white" data-translatekey="bangla-etc-text"><strong data-translatekey="bn">Bangla </strong>Medium
                                Schools and </p>
                        </div>
                        <div class="funfact">
                            <h3><span class="odometer odometer-auto-theme" data-count="175">
                                        <div class="odometer-inside"><span class="odometer-digit">
                                                <span class="odometer-digit-spacer">8</span><span
                                                        class="odometer-digit-inner">
                                                    <span class="odometer-ribbon"><span class="odometer-ribbon-inner">
                                                            <span class="odometer-value">1</span></span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="odometer-digit"><span class="odometer-digit-spacer">10</span>
                                                <span class="odometer-digit-inner"><span class="odometer-ribbon"><span
                                                                class="odometer-ribbon-inner"><span
                                                                    class="odometer-value">8</span></span>
                                                    </span>
                                                </span>
                                            </span>
                                            <span class="odometer-digit"><span
                                                        class="odometer-digit-spacer">10</span><span
                                                        class="odometer-digit-inner"><span class="odometer-ribbon"><span
                                                                class="odometer-ribbon-inner"><span
                                                                    class="odometer-value">0</span></span>
                                                    </span>
                                                </span>
                                            </span>
                                        </div>
                                    </span>
                            </h3>
                            <p class="text-white" data-translatekey="english-etc-text"><strong data-translatekey="en">English</strong>
                                Medium Schools currently don’t use LMS</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="circle-box"><img src="assets/img/circle.png" alt="image"></div>
        <div class="cta-shape"><img src="assets/img/cta-shape.png" alt="image"></div>
    </div>
</section>

<!-- Start Blog Area -->
<section class="blog-area ptb-80">
    <div class="container">
        <div class="section-title">
            <h2 data-translatekey="needy-school-txt">School We Want To Help Now</h2>
            <div class="bar"></div>
            <div class="row p-2">
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-post">
                        <div class="blog-image">
                            <a href="#">
                                <img src="assets/img/blog-image/11.jpg" class="blogImg" alt="image">
                            </a>

                            <!--<a class="date">
                            Sponsor Now
                        </a>-->
                        </div>

                        <div class="blog-post-content">
                            <h3><a href="#" data-translatekey="aysah-begum-school-txt">Ayesha Begum Girls High
                                    School</a></h3>

                            <!-- <span>by <a href="#">admin</a></span> -->

                            <a href="#" class="read-more-btn">
                                <font data-translatekey="sponsor-now">Sponsor now</font> <i
                                        data-feather="arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-post">
                        <div class="blog-image">
                            <a href="#">
                                <img src="assets/img/blog-image/22.jpg" class="blogImg" alt="image">
                            </a>

                            <!--<a class="date">
                            Sponsor Now
                        </a>-->
                        </div>

                        <div class="blog-post-content">
                            <h3><a href="#" data-translatekey="aziz-school-txt">Professor M A Aziz Ideal School</a></h3>

                            <!-- <span>by <a href="#">smith</a></span> -->

                            <!--<p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                            gravida.</p>-->

                            <a href="#" class="read-more-btn">
                                <font data-translatekey="sponsor-now">Sponsor now</font> <i
                                        data-feather="arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-post">
                        <div class="blog-image">
                            <a href="#">
                                <img src="assets/img/blog-image/33.jpg" class="blogImg" alt="image">
                            </a>

                            <!--<a class="date">
                            Sponsor Now
                        </a>-->
                        </div>

                        <div class="blog-post-content">
                            <h3><a href="#" data-translatekey="hydrabad-school-txt">Hyderabad Haji Yakub Ali High
                                    School</a></h3>

                            <!-- <span>by <a href="#">smith</a></span> -->

                            <!--<p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                            gravida.</p>-->

                            <a href="#" class="read-more-btn">
                                <font data-translatekey="sponsor-now">Sponsor now</font> <i
                                        data-feather="arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog Area -->


<section class="pricing-area ptb-80 bg-f9f6f6" id="Contact">

    <div class="contact-area ptb-80">
        <div class="container">
            <div class="section-title">
                <h2 data-translatekey="get-in-touch">Get In Touch With Us</h2>
                <div class="bar"></div>
                <p data-translatekey="get-in-touch-desc">Anything On your Mind. We’ll Be Glad To Assist You!</p>
            </div>
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-lg-6 col-md-12">
                    <img src="assets/img/students jump.png" alt="image"/>
                </div>
                <div class="col-lg-6 col-md-12">
                    <form id="contactForm" novalidate="true">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" required=""
                                           data-error="Please enter your name" placeholder="Name"/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" required=""
                                           data-error="Please enter your email" placeholder="Email"/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="phone_number" id="phone_number" required=""
                                           data-error="Please enter your number" class="form-control"
                                           placeholder="Phone"/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="msg_subject" id="msg_subject" class="form-control"
                                           required="" data-error="Please enter your subject" placeholder="Subject"/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" id="message" cols="30" rows="5"
                                              required="" data-error="Write your message"
                                              placeholder="Your Message"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="btn btn-primary disabled"
                                        style="pointer-events: all; cursor: pointer">
                                    Send Message
                                </button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Start Footer Area -->
<footer class="footer-area bg-f7fafd" id="Footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">

                    <div class="single-footer-widget">
                        <h3 data-translatekey="developed__by">Developed By</h3>
                        <div class="logo">
                            <a href="https://www.medinatech.co/"><img src="assets/img/mt-2.png" alt="logo"></a>
                        </div>

                    </div>

            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3 data-translatekey="address">Location</h3>

                    <ul class="footer-contact-info">
                        <li>
                            <i data-feather="map-pin"></i>
                            <label data-translatekey="location_1">House #25, Road #4, Block #F</label>
                            <i data-feather="map-pin"></i>
                            <label data-translatekey="location_2">Banani, Dhaka 1213</label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3 data-translatekey="foot-contact">Contact</h3>

                    <ul class="footer-contact-info mb-0">
                        <li>
                            <i data-feather="mail"></i>
                            <label data-translatekey="email-txt"></label>
                            <a data-translatekey="email" href="mailto:support@medinatech.co">support@medinatech.co</a>
                        </li>
                        <li>
                            <i data-feather="phone-call"></i>
                            <label data-translatekey="phone-txt"></label>
                            <a href="tel:+8809638600700" data-translatekey="phone">+88 0963 8600 700</a>
                        </li>
                    </ul>
                    <ul class="social-links">
                        <li>
                            <a href="https://www.facebook.com/Class-Connect-100120898989894" class="facebook"><i
                                        data-feather="facebook" aria-level="facebook"></i></a></li>
                        <!-- <li><a href="#" class="twitter"><i data-feather="twitter"></i></a></li> -->
                        <!-- <li><a href="#" class="instagram"><i data-feather="instagram"></i></a></li> -->
                        <!--                            <li><a href="#" class="linkedin"><i data-feather="linkedin" aria-level="linkedin"></i></a></li>-->
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3 data-translatekey="support">Quick link</h3>
                    <ul class="list">
                        <!-- <li><a href="#" data-translatekey="faq">FAQ's</a></li> -->
                        <li><a href="terms.php" data-translatekey="user-terms">User Terms</a></li>
                        <!-- <li><a href="#" data-translatekey="community">Community</a></li> -->
                        <!-- <li><a href="#" data-translatekey="contact-us">Contact Us</a></li> -->
                    </ul>
                </div>
            </div>



            <div class="col-lg-12 col-md-12">
                <div class="copyright-area">
                    <p data-translatekey="footer-text">
                        Copyright © 2020-2021, Medina tech. All Rights Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <img src="assets/img/map.png" class="map" alt="map">
    <div class="shape1"><img src="assets/img/shape1.png" alt="shape"></div>
    <div class="shape8 rotateme"><img src="assets/img/shape2.svg" alt="shape"></div>
</footer>
<!-- End Footer Area -->

<div class="go-top"><i data-feather="arrow-up"></i></div>

<!-- Jquery Min JS -->
<script src="assets/js/jquery.min.js"></script>
<!-- Popper Min JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap Min JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Meanmenu Min JS -->
<script src="assets/js/jquery.meanmenu.min.js"></script>
<!-- WOW Min JS -->
<script src="assets/js/wow.min.js"></script>
<!-- Magnific Popup Min JS -->
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<!-- Appear Min JS -->
<script src="assets/js/jquery.appear.js"></script>
<!-- Odometer Min JS -->
<script src="assets/js/odometer.min.js"></script>
<!-- Slick Min JS -->
<script src="assets/js/slick.js"></script>
<!-- Owl Carousel Min JS -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Feather Icon Min JS -->
<script src="assets/js/feather.min.js"></script>
<!-- Form Validator Min JS -->
<script src="assets/js/form-validator.min.js"></script>
<!-- Contact Form Min JS -->
<script src="assets/js/contact-form-script.js"></script>
<!-- StartP Map JS FILE -->
<script src="assets/js/startp-map.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>
<script>
    $(document).ready(function () {

        typing(0, $('.typewriter-text').data('translatekey'));

        function typing(index, text) {

            var textIndex = 1;

            var tmp = setInterval(function () {
                if (textIndex < text[index].length + 1) {
                    $('.typewriter-text').text(text[index].substr(0, textIndex));
                    textIndex++;
                } else {
                    setTimeout(function () {
                        deleting(index, text)
                    }, 2000);
                    clearInterval(tmp);
                }

            }, 150);

        }

        function deleting(index, text) {

            var textIndex = text[index].length;

            var tmp = setInterval(function () {

                if (textIndex + 1 > 0) {
                    $('.typewriter-text').text(text[index].substr(0, textIndex));
                    textIndex--;
                } else {
                    index++;
                    if (index == text.length) {
                        index = 0;
                    }
                    typing(index, text);
                    clearInterval(tmp);
                }

            }, 150)

        }

    });
</script>
<script>
    $(document).ready(function () {

        typing(0, $('.typewriter-text-bd').data('text'));

        function typing(index, text) {

            var textIndex = 1;

            var tmp = setInterval(function () {
                if (textIndex < text[index].length + 1) {
                    $('.typewriter-text-bd').text(text[index].substr(0, textIndex));
                    textIndex++;
                } else {
                    setTimeout(function () {
                        deleting(index, text)
                    }, 2000);
                    clearInterval(tmp);
                }

            }, 150);

        }

        function deleting(index, text) {

            var textIndex = text[index].length;

            var tmp = setInterval(function () {

                if (textIndex + 1 > 0) {
                    $('.typewriter-text-bd').text(text[index].substr(0, textIndex));
                    textIndex--;
                } else {
                    index++;
                    if (index == text.length) {
                        index = 0;
                    }
                    typing(index, text);
                    clearInterval(tmp);
                }

            }, 150)

        }

    });
</script>
<script src="assets/js/tinyi18n.js"></script>
<script>
    tinyi18n.loadTranslations('language.json');
</script>

<script>
    function englishMode() {
        tinyi18n.setLang('en');

        document.getElementById("tt-en").classList.remove('tt-none')
        document.getElementById("tt-bn").classList.add('tt-none')
    }
</script>
<script>
    function banglaMode() {
        tinyi18n.setLang('bd');

        document.getElementById("tt-bn").classList.remove('tt-none')
        document.getElementById("tt-en").classList.add('tt-none')
    }
</script>
<!-- Messenger Chat Plugin Code -->
<div id="fb-root"></div>

<!-- Your Chat Plugin code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<!-- Messenger Chat Plugin Code -->
<div id="fb-root"></div>

<!-- Your Chat Plugin code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "100120898989894");
    chatbox.setAttribute("attribution", "biz_inbox");
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v11.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>

</html>